import configparser
from pathlib import Path

from colorama import Fore

from src.utils.vpath import VPath

version = '0.1.0-beta'


class ConfigurationManager:
    def __init__(self, file_path=None):
        self.file_path = VPath.get_root() / 'config.ini' if file_path is None else file_path

        if self.file_path.is_file():
            self.config = self.load_configurations()
            if not self.config:
                self.create_default_configurations()
        else:
            self.config = self.load_configurations()
            self.create_default_configurations()

    def load_configurations(self):
        config = configparser.ConfigParser()
        try:
            config.read(self.file_path)
            self.config = config
            return config
        except FileNotFoundError:
            print(f"El archivo de configuración '{self.file_path}' no existe.")
            return None

    def save_configurations(self):
        try:
            with open(self.file_path, "w") as config_file:
                self.config.write(config_file)
            print(f"{Fore.GREEN}Configuraciones guardadas en '{self.file_path}'")
        except Exception as e:
            print(f"Error al guardar las configuraciones en '{self.file_path}': {e}")

    def get(self, sections, option, default=None):
        # self.config = self.load_configurations()
        try:
            values = self.load_configurations().get(sections, option)
            # Intenta convertir el valor a una lista
            if ',' in values:
                value_list = values.split(',')
                # Intenta convertir cada elemento de la lista a su tipo correspondiente
                converted_list = []
                for item in value_list:
                    try:
                        converted_list.append(int(item))
                    except ValueError:
                        if item.strip().lower() == "true":
                            converted_list.append(True)
                        elif item.strip().lower() == "false":
                            converted_list.append(False)
                        else:
                            try:
                                converted_list.append(float(item))
                            except ValueError:
                                converted_list.append(item.strip())
                return converted_list
            # Si no es una lista, intenta convertir el valor a int
            try:
                return int(values)
            except ValueError:
                # Intenta convertir el valor a bool
                if values.lower() == "true":
                    return True
                elif values.lower() == "false":
                    return False
                else:
                    # Si no es int ni bool, devuelve el valor como string
                    try:
                        return float(values)
                    except ValueError:
                        return values
        except (configparser.NoSectionError, configparser.NoOptionError):
            return default

    def items(self, sections):
        return self.config.items(sections)

    def sections(self):
        return self.config.sections()

    def set(self, sections: str, option: str, values: str):
        print('input', sections, option, values)
        self.config.set(sections, option, values)
        self.save_configurations()

    def create_default_configurations(self):
        # Crear configuraciones por defecto
        self.config["Archivos"] = {
            "ip_port_file": "ip_port.cjson",
            "success_file": "conexiones_exitosas.txt",
            "failure_file": "conexiones_fallidas.txt"
        }
        self.config["Desarrollador"] = {
            "Mode": "False",
            "Debug": "False",
            "StackTrace": "False",
            "Profiler": "False"
        }
        self.config["Reportes"] = {
            "crear_reporte": "True"
        }

        self.config["Extra"] = {
            "order_ips": "True",
            "order_by_ips": "asc"  # asc / desc
        }
        # Guardar configuraciones por defecto en el archivo
        self.save_configurations()


# Ejemplo de uso
config_manager = ConfigurationManager()

if __name__ == '__main__':
    config_manager.create_default_configurations()
