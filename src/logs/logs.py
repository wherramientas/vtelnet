import datetime
import inspect
import os
import traceback

from unidecode import unidecode

from src.config.settings import config_manager
from src.const.colors import RED, GREEN, CYAN, ORANGE, RESET, YELLOW, BLUE
from src.utils.regx import delete_color_codes
from src.utils.vpath import VPath


class VLogger:
    """
    Clase para gestionar registros de mensajes con diferentes niveles de severidad y opciones de registro.

    Args:
        log_file_prefix (str): Prefijo del nombre del archivo de registro.
        log_level (str): Nivel de registro predeterminado ('info' por defecto).
        custom_colors (dict): Diccionario con colores personalizados para niveles de registro.
        log_file_max_size (str): Tamaño máximo del archivo de registro antes de rotar ('1MB' por defecto).
        log_file_backup_count (int): Número máximo de archivos de registro de respaldo a mantener (2 por defecto).
        formatt (str): Formato personalizado para los mensajes de registro.
        write_file (bool): Indica si se deben escribir los registros en archivos (predeterminado: False).

    Attributes:
        log_levels (dict): Diccionario que asigna colores a los niveles de registro.
        format (str): Formato de registro predeterminado.
        debug_mode (bool): Indica si se está ejecutando en modo de depuración.
        last_rotation_date (datetime.date): Última fecha en que se rotó el archivo de registro.

    Methods:
        log(message, level='info', write=False):
            Registra un mensaje con el nivel especificado en un formato de columna.

        debug(message):
            Registra un mensaje de depuración.

        info(message, write=False):
            Registra un mensaje informativo.

        warning(message):
            Registra un mensaje de advertencia.

        error(message):
            Registra un mensaje de error.

        critical(message):
            Registra un mensaje crítico.

    """
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(VLogger, cls).__new__(cls)
            cls._instance._init(*args, **kwargs)
        return cls._instance

    def _init(self, log_folder=None, log_file_prefix="log", log_level='info', custom_colors=None,
              log_file_max_size="1MB",
              log_file_backup_count=2, formatt=None, write_file=False, write_console=False):

        self.log_file_name = None
        self.log_folder = VPath.get_root() / 'logs' if log_folder is None else log_folder
        self.log_file_prefix = log_file_prefix
        self.log_level = log_level
        self.custom_colors = custom_colors or {}
        self.write_file = write_file
        self.write_console = write_console
        self.debug_mode = config_manager.get('Desarrollador', 'debug',
                                             default=True)  # Obtener la configuración de debug
        self.last_rotation_date = None

        self.log_levels = {
            'info': GREEN,  # Verde
            'debug': CYAN,  # Cian
            'warning': YELLOW,  # Amarillo
            'error': ORANGE,  # Rojo
            'critical': RED,  # Amarillo
            'reset': RESET  # Resetear el color
        }

        self.format = formatt or (
            "{color}[{timestamp}] [{level:^8}] "
            "[FILE: {file_name}], "
            "[CLASS: {class_name} \t| "
            "METHOD: {method_name} \t| "
            "LINE: {line_number:<4}]"
            " → "
            "{message}"
            f"{self.log_levels['reset']}"
        )

        os.makedirs(self.log_folder, exist_ok=True)

        self.d = self.debug
        self.i = self.info
        self.w = self.warning
        self.e = self.error
        self.c = self.critical

    @staticmethod
    def _parse_size(size_str):
        size_str = size_str.lower()
        if size_str.endswith('kb'):
            return int(size_str[:-2]) * 1024
        elif size_str.endswith('mb'):
            return int(size_str[:-2]) * 1024 * 1024
        elif size_str.endswith('gb'):
            return int(size_str[:-2]) * 1024 * 1024 * 1024
        else:
            raise ValueError("Tamaño no válido. Debe ser una cadena como '5MB', '1024KB', etc.")

    def should_log(self, level):
        levels = list(self.log_levels.keys())
        return levels.index(level) >= levels.index(self.log_level)

    def should_rotate_log(self):
        today = datetime.date.today()
        if self.last_rotation_date != today:
            self.last_rotation_date = today
            return True
        return False

    def get_color_for_level(self, level):
        color = self.log_levels.get(level, self.custom_colors.get(level, self.log_levels['info']))
        return color

    def log(self, message, level='info', write=False):
        """
        Registra un mensaje con el nivel especificado en un formato de columna.

        Args:
            message (str): El mensaje a registrar.
            level (str): El nivel de registro ('info', 'debug', 'warning', 'error', 'critical').
            write (bool): Indica si se debe escribir el registro en el archivo (predeterminado: False).

        """
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        log_level = self.get_color_for_level(level)

        caller_frame = inspect.stack()[1]
        file_name = os.path.basename(caller_frame[1])
        line_number = caller_frame[2]
        method_name = caller_frame[3]
        class_name = caller_frame[0].f_globals.get('__name__')

        formatted_message = self.format.format(
            color=log_level,
            timestamp=timestamp,
            level=level.upper(),
            message=message,
            file_name=file_name,
            line_number=line_number,
            class_name=class_name,
            method_name=method_name.upper()
        )

        formatted_message += '\n'

        if self.should_log(level):
            if self.write_console:
                self._write_to_console(formatted_message)
            if self.write_file or write:
                self._write_to_file(formatted_message)

    def debug(self, message, write=False):
        """
        Registra un mensaje de depuracion.

        Args:
            message (str): El mensaje informativo a registrar.
            write (bool): Indica si se debe escribir el registro en el archivo (predeterminado: False).

        """
        self.log(message, level='debug', write=write)

    def info(self, message, write=False):
        """
        Registra un mensaje informativo.

        Args:
            message (str): El mensaje informativo a registrar.
            write (bool): Indica si se debe escribir el registro en el archivo (predeterminado: False).

        """
        self.log(message, write=write)

    def warning(self, message, write=False):
        """
        Registra un mensaje advertencia.

        Args:
            message (str): El mensaje informativo a registrar.
            write (bool): Indica si se debe escribir el registro en el archivo (predeterminado: False).

        """
        self.log(message, level='warning', write=write)

    @staticmethod
    def _format_stacktrace(mensaje, stacktrace, compiler):
        if compiler:
            # traceback_str = traceback.format_exc()
            return f"{mensaje}\n{traceback.format_exc()}"

        if stacktrace and not compiler:
            return f"{mensaje}\n{stacktrace}"

        return mensaje

    def error(self, message, write=False, stacktrace=None, autotrace=False):
        """
        Registra un mensaje error.

        Args:
            :param write: (bool) Indica si se debe escribir el registro en el archivo (predeterminado: False).
            :param message: (str) El mensaje informativo a registrar.
            :param stacktrace: (bool)
        """
        self.log(VLogger._format_stacktrace(message, stacktrace, autotrace), level='error', write=write)

    def critical(self, message, write=False, stacktrace=None, autotrace=False):
        """
        Registra un mensaje criticos del sistema.

        Args:
            :param write: (bool) Indica si se debe escribir el registro en el archivo (predeterminado: False).
            :param message: (str) El mensaje informativo a registrar.
            :param stacktrace: (bool)

        """
        self.log(VLogger._format_stacktrace(message, stacktrace, autotrace), level='critical', write=write)

    def _write_to_console(self, message):
        if not self.debug_mode:
            return  # No imprimir si el modo de depuración está desactivado
        print(message, end='')

    def _write_to_file(self, message):
        if self.log_file_name is None or self.should_rotate_log():
            self.log_file_name = f"{self.log_file_prefix}_{datetime.date.today()}.log"
        try:
            log_file_message = delete_color_codes(message)
            log_file_message = unidecode(log_file_message)

            with open(self.log_file_name, 'a') as log_file:
                log_file.write(log_file_message)

        except Exception as e:
            print(f"Error al escribir en el archivo de registro: {str(e)}")


if __name__ == '__main__':
    # Crear una instancia de VLogger
    logger = VLogger()

    # Habilitar la captura de excepciones no controladas

    try:
        # Intencionalmente induce una excepción no controlada
        result = 1 / 0  # Esto generará una excepción ZeroDivisionError
    except Exception as e:
        # No atrapamos la excepción aquí, pero se capturará y registrará automáticamente
        logger.e('Error', stacktrace=e)
        logger.c('Critico', autotrace=True)

    # También puedes generar otra excepción no controlada para ver si se registra
    # raise Exception("Esto es una excepción no controlada")

    # Deshabilitar la captura de excepciones no controladas si ya no se necesita
    # logger.disable_unhandled_exception_logging()
