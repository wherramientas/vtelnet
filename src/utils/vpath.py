from pathlib import Path, PurePath


class VPath:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(VPath, cls).__new__(cls)
            # cls._instance.init_singleton()
        return cls._instance

    @staticmethod
    def get_root() -> PurePath:
        root_files = ['README.md', 'config.ini', 'main.exe', 'ip_port.json']  # Lista de archivos a buscar
        current_dir = Path(__file__).resolve().parent
        while not any((current_dir / file).is_file() for file in root_files):
            parent_dir = current_dir.parent
            if parent_dir == current_dir:
                break
            current_dir = parent_dir
        return current_dir


if __name__ == '__main__':
    path = VPath()

    print(path.get_root())
