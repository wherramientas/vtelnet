def delete_color_codes(text):
    # Elimina los códigos de color ANSI de un texto
    import re
    # color_pattern = re.compile(r'\x1b\[\d{1,2}(;\d{1,2})?m')
    color_pattern = re.compile(r'\033\[\d+(?:;\d+)*m')
    return color_pattern.sub('', text)