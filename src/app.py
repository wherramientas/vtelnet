import sys

from colorama import Fore

from src.logs.logs import VLogger
from src.network.chek_telnet import check_telnet_access
from src.console.menus import configure_settings, dynamic_menu
from src.config.settings import config_manager, version
from src.json.wjson import JSONManager

logger = VLogger()


class App:
    default_data = '{\n// "10": [ { "1": [ { "99": [ { "141": [ 64234 ] } ] } ] } ]\n}'

    def __init__(self):
        self.file_path = config_manager.config["Archivos"]["ip_port_file"] or "ip_port.cjson"
        self.json_manager = JSONManager(self.file_path, self.default_data)
        self.data = self.json_manager.load_json_file()

    def validar_conexion(self, write=False):
        exitosos = errores = 0
        success_file = config_manager.config["Archivos"]["success_file"] or "conexiones_exitosas.txt"
        failure_file = config_manager.config["Archivos"]["failure_file"] or "conexiones_fallidas.txt"

        print("Verificar conexiones...")
        # Aquí puedes agregar lógica relacionada con la opción, utilizando las configuraciones si es necesario.
        if self.data is None:
            self.data = self.json_manager.load_json_file()

            print(
                f'{Fore.RED}NO SE ENCONTRO EL ARCHIVO {config_manager.config["Archivos"]["ip_port_file"]} con la '
                f'estructura\n\n{Fore.MAGENTA}{self.default_data}\n\n'
                f'{Fore.RED}Se procede a la creacion del mismo y asignacion de IPs:\n'
            )

            self.json_manager.crud_ips(self.file_path)

        ip_list = self.json_manager.get_ip_ports(self.data)
        print(f'{Fore.BLUE}Lista de IPs: ', ip_list)
        print(f'{Fore.BLUE}CANTIDAD IPs: ', len(ip_list))
        print(f"{Fore.BLUE}{'=*' * 50}")

        if write:
            with open(success_file, "w") as success_output, open(failure_file, "w") as failure_output:
                for dominio in ip_list:
                    ip, port = dominio.split(':')
                    if check_telnet_access(str(ip), int(port)):
                        if write:
                            success_output.write(f"Conexion Exitosa {ip}:{port}\n")
                        else:
                            print(f"{Fore.GREEN}Conexion Exitosa {ip}:{port}")
                        exitosos += 1
                    else:
                        if write:
                            failure_output.write(f"Error de Conexion {ip}:{port}\n")
                        else:
                            print(f"{Fore.RED}Error de Conexion {ip}:{port}")
                        errores += 1
        else:
            for dominio in ip_list:
                ip, port = dominio.split(':')
                if check_telnet_access(str(ip), int(port)):
                    print(f"{Fore.GREEN}Conexion Exitosa {ip}:{port}")
                    exitosos += 1
                else:
                    print(f"{Fore.RED}Error de Conexion {ip}:{port}")
                    errores += 1

        print(f"{Fore.BLUE}{'=*' * 50}")
        if write:
            print(f"{Fore.LIGHTGREEN_EX}Se generó el informe exitoso en '{success_file}' con {exitosos} "
                  f"conexiones exitosas.")
            print(f"{Fore.LIGHTRED_EX}Se generó el informe de fallas en '{failure_file}' con {errores} "
                  f"conexiones fallidas.\n")
        else:
            print(f"{Fore.LIGHTGREEN_EX}Exitosos {exitosos}")
            print(f"{Fore.LIGHTRED_EX}Errores  {errores}\n")

    def generate_report(self):
        print("Generando informe...")
        print(f"{Fore.BLUE}{'=*' * 50}")
        self.validar_conexion(True)
        print(f"{Fore.BLUE}{'=*' * 50}")

    @staticmethod
    def access_settings():
        print("Accediendo a configuraciones...")
        configure_settings()  # Acceder a las configuraciones

    def configure_ips(self):
        print("MENU DE IPS:")
        print("1. Listado de IPs")
        print("2. Agregar Ip")
        print("3. Modificar Ip")
        print("4. Eliminar Ip")
        print("5. Regresar")

        choice = input(f"{Fore.BLUE}Seleccione una opción: ")

        if choice == "1":
            self.list_ips()
        elif choice == "2":
            self.add_ips()
        elif choice == "3":
            self.modf_ips()
        elif choice == "4":
            self.del_ips()
        elif choice == "5":
            return
        else:
            print(f"{Fore.RED}Opción no válida")

    def list_ips(self):
        ip_list = self.json_manager.get_ip_ports(self.data)
        print(f"{Fore.BLUE}{'=*' * 50}")
        for i, ip in enumerate(ip_list):
            print(f"{Fore.CYAN}{i + 1}. {ip}")
        print(f"{Fore.BLUE}{'=*' * 50}")

    def add_ips(self):
        if self.data is None:
            self.data = self.json_manager.load_json_file()

            print(
                f'{Fore.RED}NO SE ENCONTRO EL ARCHIVO {config_manager.config["Archivos"]["ip_port_file"]} con la '
                f'estructura\n\n{Fore.MAGENTA}{self.default_data}\n\n'
                f'{Fore.RED}Se procede a la creacion del mismo y asignacion de IPs:\n'
            )

        self.json_manager.add_ips(self.file_path)

    def modf_ips(self):
        if self.data is None:
            self.data = self.json_manager.load_json_file()

            print(
                f'{Fore.RED}NO SE ENCONTRO EL ARCHIVO {config_manager.config["Archivos"]["ip_port_file"]} con la '
                f'estructura\n\n{Fore.MAGENTA}{self.default_data}\n\n'
                f'{Fore.RED}Se procede a la creacion del mismo y asignacion de IPs:\n'
            )
        self.json_manager.upd_ips(self.file_path)

    def del_ips(self):
        if self.data is None:
            self.data = self.json_manager.load_json_file()

            print(
                f'{Fore.RED}NO SE ENCONTRO EL ARCHIVO {config_manager.config["Archivos"]["ip_port_file"]} con la '
                f'estructura\n\n{Fore.MAGENTA}{self.default_data}\n\n'
                f'{Fore.RED}Se procede a la creacion del mismo y asignacion de IPs:\n'
            )
        self.json_manager.del_ips(self.file_path)

    @staticmethod
    def info():
        print(f"{Fore.GREEN}{'=' * 100}")
        print(f"{Fore.GREEN}Información del Software")
        print(f"{Fore.GREEN}Autor: Walter Cun Bustamante")
        print(f"{Fore.GREEN}Correo: waltercunbustamante@gmail.com")
        print(f"{Fore.GREEN}Versión: {version}")
        print(f"{Fore.GREEN}Descripción: Un software para validar acceso por telnet a una colección de IPs y puertos.")
        print(f"{Fore.GREEN}{'=' * 100}\n")
        input('PAUSE... PRESS ENTER')

    def start(self):

        menu_options = {
            "IPs": (True, self.configure_ips),
            "Verificar conexiones": (True, self.validar_conexion),
            "Generar informe": (config_manager.get("Reportes", "crear_reporte"), self.generate_report),
            "Configuraciones": (True, self.access_settings),
            "Informacion": (True, self.info),
        }

        if config_manager.get('Desarrollador', 'mode'):
            if not config_manager.get('Desarrollador', 'stacktrace'):
                logger_function = logger.e
            else:
                logger_function = logger.e
        else:
            logger_function = logger.e

        try:
            dynamic_menu(menu_options)
        except Exception as e:
            logger_function('Excepcion no controlada', stacktrace=e)
            logger.c('StackTrace', autotrace=True)
            sys.exit(999)

        self.start()


if __name__ == '__main__':
    app = App()
    app.add_ips()
