from src.const.colors import YELLOW, GREEN, RED


class VColor:
    RESET = '\033[0m'

    @staticmethod
    def apply(text, color):
        """
        Aplica un color a un texto.

        :param text: El texto al que se aplicará el color.
        :param color: El código de color (por ejemplo, Color.RED).
        :return: El texto con el color aplicado.
        """
        return f"{color}{text}{VColor.RESET}"

    @staticmethod
    def colorize(text, foreground_color, background_color=None):
        """
        Aplica colores de texto y fondo a un texto.

        :param text: El texto al que se aplicarán los colores.
        :param foreground_color: El código de color para el texto (por ejemplo, Color.RED).
        :param background_color: El código de color para el fondo (opcional, por ejemplo, Color.GREEN).
        :return: El texto con los colores aplicados.
        """
        if background_color:
            return f"{foreground_color}{background_color}{text}{VColor.RESET}"
        return f"{foreground_color}{text}{VColor.RESET}"

    @staticmethod
    def colorized(text, universe_color):
        """
        Aplica colores de texto y fondo a un texto.

        :param text: El texto al que se aplicarán los colores.
        :param universe_color: El código de color personalizado (por ejemplo, '\033[38;5;100m').
        :return: El texto con los colores aplicados.
        """
        return f"{universe_color}{text}{VColor.RESET}"

    @staticmethod
    def generate(fg, bg=None, bold=False, italics=False, underlined=False):
        """
        Genera un nuevo código de color personalizado.

        :param fg: Código de color para el texto (por ejemplo, '31' para rojo).
        :param bg: Código de color para el fondo (opcional, por ejemplo, '42' para verde).
        :param bold: True si se debe aplicar negrita, False en caso contrario.
        :param italics: True si se debe aplicar cursiva, False en caso contrario.
        :param underlined: True si se debe aplicar subrayado, False en caso contrario.
        :return: El código de color personalizado.
        """
        ESCAPE = '\033['
        END = 'm'
        code_txt = ''

        if bold:
            code_txt += '1'

        if italics:
            code_txt += '2'

        if underlined:
            code_txt += '4'

        code = (
            f"{ESCAPE}"
            f"{code_txt}"
            f"{';' if code_txt else ''}"
            f"{('4' + bg) if bg is not None else ''}"
            f"{';' if code_txt or bg is not None else ''}"
            f"{'3' + fg}"
            f"{END}"
        )

        return code


if __name__ == '__main__':
    # Aplicar un color al texto
    colored_text = VColor.apply("Texto en rojo", RED)
    print(colored_text)

    # Aplicar colores de texto y fondo al texto
    colored_text = VColor.colorize("Texto en verde sobre fondo amarillo", GREEN, YELLOW)
    print(colored_text)

    GREEN_BLUE = VColor.generate(fg='7')
    print(VColor.colorized("Texto en verde sobre fondo amarillo", GREEN_BLUE))

    print(colored_text)