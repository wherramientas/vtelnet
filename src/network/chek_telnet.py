import socket

from src.decoratos.profilers import performance_time


@performance_time
def check_telnet_access(ip: str, port: int):
    try:
        # Intenta conectar al IP y puerto dado
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(5)  # Tiempo de espera en segundos
            s.connect((ip, port))
        return True  # La conexión tuvo éxito
    except (socket.timeout, ConnectionRefusedError):
        return False  # No se pudo conectar al IP y puerto