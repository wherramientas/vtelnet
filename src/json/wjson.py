import json
import re
from colorama import Fore
from jsoncomment import JsonComment


class JSONManager:
    def __init__(self, file_path, default=None):
        self.json_parser = JsonComment()
        self.file_path = file_path
        self.default = default
        self.data = None

    def get_ip_ports(self, data, current_ip="", level=0):
        ip_ports = []  # Lista para almacenar las direcciones IP y puertos encontrados
        level += 1  # Incrementa el nivel de anidación

        if isinstance(data, dict):
            # Si el valor actual es un diccionario, significa que estamos en un nivel de anidación
            for key, value in data.items():
                if isinstance(value, list):
                    # Si el valor es una lista, iteramos a través de ella
                    for ip_or_port in value:
                        # Llamamos recursivamente a la función para procesar el valor
                        # Concatenamos la clave actual con la dirección IP o puerto
                        # Utilizamos '.' como separador si el nivel de anidación es menor que 4, de lo contrario,
                        # usamos ':'
                        ip_ports.extend(
                            self.get_ip_ports(ip_or_port, current_ip + f"{key}{'.' if level < 4 else ':'}", level))
        else:
            # Si el valor actual no es un diccionario, entonces es una dirección IP o puerto
            # Concatenamos la clave actual con el valor y lo agregamos a la lista
            ip_ports.append(f'{current_ip}{data}')

        return ip_ports  # Devolvemos la lista de direcciones IP y puertos encontrados

    @staticmethod
    def remove_comments(json_str):
        # Expresión regular para eliminar comentarios en estilo // y /* ... */
        pattern = r"(\/\/[^\n]*|\/\*[\s\S]*?\*\/)"
        return re.sub(pattern, "", json_str)

    def load_json_file(self):
        try:
            with open(self.file_path, "r") as json_file:
                self.data = self.json_parser.load(json_file)
            return self.data
        except FileNotFoundError:
            if self.default is not None:
                with open(self.file_path, "w") as json_file:
                    json_file.write(self.default)
                return None
            else:
                print(f"El archivo '{self.file_path}' no existe y no se proporcionaron datos predeterminados.")
                return None
        except ValueError as e:
            print(f"Error al decodificar el archivo '{self.file_path}': {e}")
            return None

    def add_ips(self, file_path: {str} = None, ip: {str} = None, port: {str} = None):
        if None is self.data:
            print(f"{Fore.RED}El archivo no existe o está vacío.")
            return

        # Solicitar al usuario la IP y el puerto si no se proporcionaron como argumentos
        if not ip:
            ip = input("Ingrese la IP (en formato x.x.x.x): ")
        if not port:
            port = input("Ingrese el puerto: ")

        try:
            ip_parts = [int(part) for part in ip.split('.')]
        except ValueError:
            print(f"{Fore.RED}El formato de la IP ingresada no es correcto.")
            return

        if len(ip_parts) != 4:
            print(f"{Fore.RED}La IP ingresada no tiene el formato correcto.")
            return

        if not port.isdigit():
            print(f"{Fore.RED}El puerto debe ser un número válido.")
            return

        port = int(port)

        # Construir la estructura de datos anidada basada en la IP
        current_data = self.data
        for ip_part in ip_parts[:-1]:
            if str(ip_part) not in current_data:
                current_data[str(ip_part)] = [{}]
            current_data = current_data[str(ip_part)][0]

        # Verificar si el puerto ya existe
        if port in current_data.get(str(ip_parts[-1]), []):
            print(
                f"{Fore.LIGHTRED_EX}La IP {ip} con el puerto {port} ya se registró anteriormente."
            )
            return
        else:
            current_data.setdefault(str(ip_parts[-1]), []).append(port)

        # Guardar los cambios en el archivo
        with open(file_path or self.file_path, "w") as json_file:
            json.dump(self.data, json_file, indent=2)
        print(
            f"{Fore.GREEN}Se ha agregado la IP {ip} con el puerto {port} al archivo {self.file_path}."
        )

    def del_ips(self, file_path: {str} = None, ip: {str} = None, port: {str} = None):
        if None is self.data:
            print(f"{Fore.RED}El archivo no existe o está vacío.")
            return

            # Solicitar al usuario la IP y el puerto si no se proporcionaron como argumentos
        if not ip:
            ip = input("Ingrese la IP (en formato x.x.x.x) a eliminar: ")
        if not port:
            port = input("Ingrese el puerto a eliminar: ")

        try:
            ip_parts = [int(part) for part in ip.split('.')]
        except ValueError:
            print(f"{Fore.RED}El formato de la IP ingresada no es correcto.")
            return

        if len(ip_parts) != 4:
            print(f"{Fore.RED}La IP ingresada no tiene el formato correcto.")
            return

        if not port.isdigit():
            print(f"{Fore.RED}El puerto debe ser un número válido.")
            return

        port = int(port)

        # Buscar y eliminar la IP y puerto
        current_data = self.get_ip_ports(self.data)

        for ips in current_data:
            if f'{ip}:{port}' == ips:
                current_data.remove(ips)

        self.truc_ips()

        for ips in current_data:
            i, p = ips.split(':')
            self.add_ips(ip=i, port=p)

    def upd_ips(self):
        pass

    def truc_ips(self, file_path: {str} = None):
        with open(file_path or self.file_path, "w") as json_file:
            json.dump({}, json_file, indent=2)



