import time


def performance_time(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()

        elapsed_time = end_time - start_time

        # Convierte el tiempo transcurrido en formato "hh:mm:ss"
        hours, remainder = divmod(elapsed_time, 3600)
        minutes, seconds = divmod(remainder, 60)

        time_str = f"{int(hours):02d}:{int(minutes):02d}:{int(seconds):02d}"

        print(f"Tiempo de ejecicion {time_str}")

        return result

    return wrapper


# Uso del decorador
if __name__ == '__main__':
    @delay
    def example_function():
        # Simula una función que toma tiempo en ejecutarse
        time.sleep(2)


    example_function()

