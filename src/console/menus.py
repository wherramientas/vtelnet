import sys
from colorama import init, Fore
from src.config.settings import config_manager

# Inicializa colorama
init(autoreset=True)


def dynamic_menu(options):
    enabled_options = [(option, action) for option, (enabled, action) in options.items() if enabled]

    if not enabled_options:
        print("No hay opciones habilitadas.")
        sys.exit(100)

    print(f"{Fore.BLUE}Seleccione una opción: ")
    for i, (option, _) in enumerate(enabled_options, start=1):
        print(f"{i}. {option}")
    print(f"{len(enabled_options) + 1}. Salir")

    choice = input(f"{Fore.BLUE}Seleccione una opción: ")

    if choice.isdigit():
        choice = int(choice)
        if 1 <= choice <= len(enabled_options):
            _, action = enabled_options[choice - 1]
            action()
        elif choice == len(enabled_options) + 1:
            sys.exit(200)
    else:
        print(f"{Fore.RED}Opción no válida. Intente nuevamente.")


def configure_settings():
    print("Configuraciones:")
    print("1. Ver Configuraciones Actuales")
    print("2. Modificar Configuraciones")
    print("3. Restablecer Configuraciones")
    print("4. Regresar")

    def get_opctions(title, options) -> str | None:
        assert options is not None

        def prints_items():
            for i, item in enumerate(options):
                print(
                    f"{Fore.GREEN}{i + 1}. {item if type(item) is not tuple else f'{item[0]} {{Value: {item[1]}}}'}")

        prints_items()
        while True:
            opc = input(title)
            if len(opc) == 0:
                opc = None
                break
            elif opc.isdigit() and int(opc) > len(options) or not opc.isdigit():
                print(f'{Fore.RED}El item elegino no se encuentra en la lista...')
                prints_items()
            else:
                break

        if opc is None:
            return None

        return str(options[int(opc) - 1]) if type(options[int(opc) - 1]) is not tuple else options[int(opc) - 1][0]

    choice = input(f"{Fore.BLUE}Seleccione una opción: ")

    if choice == "1":
        print("Configuraciones actuales:")
        print(f'{Fore.YELLOW}{"*" * 100}')
        for section in config_manager.config.sections():
            print(f"{Fore.MAGENTA}[{section}]")
            for key, value in config_manager.config.items(section):
                print(f"  {Fore.GREEN}{key} = {Fore.CYAN}{value}")
        print(f'{Fore.YELLOW}{"*" * 100}')
        input('Pause....')
    elif choice == "2":
        section = get_opctions(title=f"{Fore.BLUE}Ingrese la sección: ",
                               options=config_manager.config.sections())

        if section is None:
            return

        option = get_opctions(f"{Fore.BLUE}Ingrese la opción que desea modificar: ", config_manager.items(section))

        if option is None:
            return

        new_value = input(f"{Fore.BLUE}Ingrese el nuevo valor: ")

        config_manager.set(section, option, new_value)
        # config_manager.save_configurations()
        print(f"La configuración '{option}' en la sección '{section}' ha sido actualizada.")
    elif choice == "3":
        continuar = get_opctions(title='Esta seguro de restablecer la configuaracion: ',
                                 options=['SI', 'NO'])
        if continuar == "SI":
            config_manager.create_default_configurations()
        else:
            print('No se restablecieron las configuraciones......')
    elif choice == "4":
        return
    else:
        print(f"{Fore.RED}Opción no válida")
