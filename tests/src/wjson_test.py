import unittest
from unittest.mock import patch
from src.json.wjson import JSONManager  # Asegúrate de importar tu clase y módulo adecuadamente


class TestJSONManager(unittest.TestCase):
    def setUp(self):
        # Puedes inicializar aquí una instancia de JSONManager para usarla en tus tests
        self.json_manager = JSONManager("test_file.cjson")

    def test_get_ip_ports(self):
        # Define un ejemplo de datos y una IP para probar la función get_ip_ports
        example_data = {
            "1": [
                {"2": [
                    {"3": [
                        {"4": [80, 443]}
                    ]}
                ]}
            ]
        }

        # Llama a la función y verifica que devuelva la lista de puertos correctamente
        result = self.json_manager.get_ip_ports(example_data)
        expected_result = ["1.2.3.4:80", "1.2.3.4:443"]
        self.assertEqual(result, expected_result)

    @patch("builtins.input", side_effect=["1.2.3.4", "80"])
    def test_crud_ips(self, mock_input):
        # Define un ejemplo de datos y una IP para probar la función crud_ips
        example_data = {
            "1": [
                {"2": [
                    {"3": [
                        {"4": [80]}
                    ]}
                ]}
            ]
        }

        # Configura los datos en la instancia de JSONManager
        self.json_manager.data = example_data

        expected_data = {
            "1": [
                {"2": [
                    {"3": [
                        {"4": [80]}
                    ]}
                ]}
            ]
        }
        self.assertEqual(self.json_manager.data, expected_data)


if __name__ == "__main__":
    unittest.main()
