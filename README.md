# VTelnet

VTelnet es una herramienta de conexión telnet multiplataforma que permite a los usuarios establecer conexiones telnet en
sistemas Windows, Linux y macOS.

## Descarga y Ejecución

A continuación, se proporcionan instrucciones para descargar y ejecutar VTelnet en diferentes sistemas operativos:

# Windows

1. [Descarga VTelnet para Windows](enlace-a-la-versión-de-Windows).
2. Ejecuta el archivo descargado para iniciar la aplicación en Windows.

# Linux

1. Abre una terminal en Linux.
2. Navega al directorio donde se encuentra el archivo VTelnet.
3. Ejecuta el siguiente comando para dar permisos de ejecución al archivo:

```bash
chmod +x VTelnet
```

### Ejecuta VTelnet:

```bash
./VTelnet
```

# macOS
Descarga VTelnet para macOS.
Abre una terminal en macOS.
Navega al directorio donde se encuentra el archivo VTelnet.
Ejecuta el siguiente comando: